FROM python:3.10.7-slim

ARG ARGO_VERSION=3.3.9
ARG YQ_VERSION=4.28.1
ARG DEFAULT_MODULE=tcd_pipeline
ARG TCD_PIPELINE_VERSION=0.2.6

ENV DEFAULT_MODULE=${DEFAULT_MODULE}
ENV PIP_ROOT_USER_ACTION=ignore

COPY entrypoint.sh /entrypoint.sh
RUN apt-get update && \
    apt-get install -y \
      curl \
      git \
      jq \
      && \
    # Install argo cli
    curl -sL -o /tmp/argo.gz https://github.com/argoproj/argo-workflows/releases/download/v${ARGO_VERSION}/argo-linux-amd64.gz && \
    gunzip /tmp/argo.gz && \
    mv /tmp/argo /usr/local/bin/ \
    ;\
    # Install yq
    curl -sL -o /usr/local/bin/yq https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_linux_amd64 && \
    \
    # Install kubectl ( latest )
    curl -sL -o /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl; \
    \
    # Make executable
    chmod +x /usr/local/bin/*; \
    chmod +x /entrypoint.sh; \
    \
    pip install tcd-pipeline==${TCD_PIPELINE_VERSION}

WORKDIR /workflows
ENTRYPOINT ["/entrypoint.sh"]
CMD [ "python", "-m", "$DEFAULT_MODULE" ]